package main

import (
	// "context"

	"bytes"
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"path/filepath"
	"strconv"
	"time"

	// "github.com/aws/aws-sdk-go/aws/awsutil"
	"os"
	"strings"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/db"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awsutil"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	_ "github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/yunabe/easycsv"
	"google.golang.org/api/option"
	//"github.com/aws/aws-sdk-go/aws/awsutil"
)

type XCALLID struct {
	CI           string
	count        int
	visit_status bool
	auto_resp    string
}

func getConfig() *aws.Config {
	aws_access_key_id := "AKIAIJ64UUC6DNZSP2GA"
	aws_secret_access_key := "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"
	token := ""
	creds := credentials.NewStaticCredentials(aws_access_key_id, aws_secret_access_key, token)

	_, err1 := creds.Get()
	if err1 != nil {
		fmt.Println("error in creds") // handle error
	}
	cfg := aws.NewConfig().WithRegion("ap-south-1").WithCredentials(creds)

	return cfg

}

func getS3() *s3.S3 {

	svc := s3.New(session.New(), getConfig())

	return svc
}
func downloadImage(f string, refkey string) {

	sess, _ := session.NewSession(getConfig())
	file, err := os.Create(f)
	if err != nil {
		exitErrorf("Unable to open file %q, %v", err)
	}

	defer file.Close()
	downloader := s3manager.NewDownloader(sess)
	numBytes, err := downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String("vishwam-fr-india"),
			Key:    aws.String(refkey),
		})
	if err != nil {
		exitErrorf("Unable to download item %q, %v", refkey, err)
	}
	fmt.Println("Downloaded", file.Name(), numBytes, "bytes")

}
func getListObject(folderpath string) *s3.ListObjectsOutput {

	svc := getS3()
	//fmt.Println(reflect.TypeOf(svc))
	bucket := "vishwam-fr-india"
	resp, err := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(bucket), Prefix: aws.String(folderpath)})
	if err != nil {
		exitErrorf("Unable to list items in bucket %q, %v", bucket, err)
	}

	return resp

}
func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	// os.Exit(1)
}

func uploadImage(f string, refkey string) {

	svc := s3.New(session.New(), getConfig())
	file, err := os.Open(f)
	if err != nil {
		exitErrorf("Unable to open file %q, %v", err)
	}

	defer file.Close()
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	buffer := make([]byte, size) // read file content to buffer

	file.Read(buffer)
	fileBytes := bytes.NewReader(buffer)
	fileType := http.DetectContentType(buffer)
	//downloader := s3manager.NewDownloader(sess)
	//numBytes, err := downloader.Download(file,
	//	&s3.PutObjectInput{
	//
	//		Bucket: aws.String("vishwam-fr-india"),
	//		Key: aws.String(refkey),
	//	})
	//if err != nil {
	//	exitErrorf("Unable to download item %q, %v", refkey, err)
	//}
	//fmt.Println("Downloaded",file.Name(), numBytes, "bytes")

	params := &s3.PutObjectInput{
		Bucket:        aws.String("vishwam-fr-india"),
		Key:           aws.String(refkey),
		Body:          fileBytes,
		ContentLength: aws.Int64(size),
		ContentType:   aws.String(fileType),
	}

	resp, err := svc.PutObject(params)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("response %s", awsutil.StringValue(resp))
}

func getFileName(keyname string) string {

	splitnames := strings.Split(keyname, "/")
	var filename string

	if len(splitnames) == 4 {
		filename = splitnames[3]
	} else {
		filename = splitnames[2]
	}

	return filename

}

func getNextListObject(folderpath string, marker string) *s3.ListObjectsOutput {

	svc := getS3()
	//fmt.Println(reflect.TypeOf(svc))
	bucket := "vishwam-fr-india"
	resp, err := svc.ListObjects(&s3.ListObjectsInput{Bucket: aws.String(bucket), Prefix: aws.String(folderpath), Marker: aws.String(marker)})
	if err != nil {
		exitErrorf("Unable to list items in bucket %q, %v", bucket, err)
	}

	return resp

}

var truncKeyList []string

func getTruncatedKeys(isTrunc bool, lastkey string, date string) []string {
	var path string = "reliance_testing/" + date
	var trunkImageKeys []string

	keys := getNextListObject(path, lastkey)
	for _, k := range keys.Contents {
		trunkImageKeys = append(trunkImageKeys, *k.Key)
	}
	for _, kk := range trunkImageKeys {
		truncKeyList = append(truncKeyList, kk)
	}
	if *keys.IsTruncated == true {
		_ = getTruncatedKeys(*keys.IsTruncated, *keys.Contents[*keys.MaxKeys-1].Key, date)
	}
	return truncKeyList

}
func isUserIdCaptured(unames []users, un string) bool {

	var scount int = 0
	//var temp string

	for p, _ := range unames {

		//fmt.Println(len(unames))
		//temp = unames[0]
		if len(unames) == 0 {
			return false
		} else if unames[p].Un == un {
			scount += 1
			unames[p].Count =  unames[p].Count + 1
		}

	}

	if scount == 1 {
		return true
	} else {
		return false
	}

}

func isXCallIdCaptured(xcallid string, callids []XCALLID) bool {
	var cicnt int = 0

	for index, ci := range callids {

		if len(callids) == 0 {
			return false
		} else if ci.CI == xcallid {
			cicnt += 1
			//ci.count += 1
			callids[index].count += 1
		}

	}

	if cicnt == 1 {
		return true
	} else {
		return false
	}
}
func getCallId(keyname string) string {

	splitnames := strings.Split(keyname, "/")
	var filename string
	if len(splitnames) != 4 {
		return "NA"
	} else {
		filename = splitnames[3]
	}

	fnsplits := strings.Split(filename, "_")
	cid := fnsplits[0]

	return cid
}
func getUniqueXcallids(list []string) []XCALLID {

	var xcallids []XCALLID
	for _, userItems := range list {

		keyname := userItems
		cid := getCallId(keyname)
		filename := getFileName(keyname)
		filesplits := strings.Split(filename, "_")
		// keysplits := strings.Split(keyname, "/")
		// var un string = keysplits[2]
		if filesplits[1] != "ref.png" {

			var callid XCALLID
			callid.CI = cid
			//fmt.Println("XCallid:",callid)
			if isXCallIdCaptured(callid.CI, xcallids) == false {

				callid.count = 1
				xcallids = append(xcallids, callid)
			}

		}

	}

	return xcallids

}
type users struct{
	Un string
	Count int
}

func getUniqueUserIds(list []string) []users {



	var usernames []users
	var usercount int = 0
	for _, item := range list {

		//keyname := item
		//k := strings.Split(keyname, "/")
		username := item
		//fmt.Println(username)

		if isUserIdCaptured(usernames, username) == false {
			var tmp users
			tmp.Un = username
			tmp.Count = 1
			usernames = append(usernames, tmp)

			usercount += 1
		}

		//fmt.Println("")
	}
	fmt.Println("No of Unique Users:", usercount)
	return usernames

}

func getTotalTransactions(date string){


	var keyList []string
	var bondUsers []string

	var path string = "reliance_testing/" + date

	image_keylist := getListObject(path)

	//var last_key string
	for _, keys := range image_keylist.Contents {

		// fmt.Println('$')
		keyList = append(keyList, *keys.Key)

	}

	if *image_keylist.IsTruncated == true {

		subKeyList := getTruncatedKeys(*image_keylist.IsTruncated, *image_keylist.Contents[*image_keylist.MaxKeys-1].Key, date)
		for _, kkkk := range subKeyList {

			// fmt.Print("@")
			keyList = append(keyList, kkkk)
		}
	}

	uci:= getUniqueXcallids(keyList)

	fmt.Println("TOtal transactions:",len(uci))

	//get username for each transactions of non mhere users

	for i,_ := range uci																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			{


		if(uci[i].CI != "NA"){
			var callid = uci[i].CI
			// fmt.Println(callid)
			type RetrieveDashboardOut struct {
				ID         string `json:"id,omitempty"`
				UserID     string `json:"userID,omitempty"`
				AppID      string `json:"appID,omitempty"`
				Path       string `json:"path,omitempty"`
				Env        string `json:"env,omitempty"`
				Properties struct {
					Path       string `json:"path,omitempty"`
					NewUser    int    `json:"newuser,omitempty"`
					StatusCode int    `json:"statusCode,omitempty"`
					// StatusCodeDB     int           `json:"statusCodeDB,omitempty"`
					AutoResp         int           `json:"autoResp,omitempty"`
					Error            string        `json:"error,omitempty"`
					FaceRatio        string        `json:"faceratio,omitempty"`
					FaceSpan         time.Duration `json:"facespan,omitempty"`
					SpoofSpan        time.Duration `json:"spoofspan,omitempty"`
					ProbabilitySpan  time.Duration `json:"probabilityspan,omitempty"`
					PeripheralSpan   time.Duration `json:"PeripheralSpan,omitempty"`
					Latency          int           `json:"latency,omitempty"`
					StartTime        time.Time     `json:"starttime,omitempty"`
					EndTime          time.Time     `json:"endtime,omitempty"`
					Version          int           `json:"version,omitempty"`
					Distance         float64       `json:"distance"`
					TypeFrom         string        `json:"typefrom"`
					Probabilities    []float64     `json:"probabilities"`
					ProbMean         float64       `json:"probMean"`
					LiveMean         float64       `json:"liveMean"`
					RefMean          float64       `json:"refMean"`
					Blur             float64       `json:"blur"`
					ProbabilitiesRef []float64     `json:"probabilitiesref"`
					Landmarks        string        `json:"landmarks"`
					LandmarksDb      [][]int       `json:"landmarksdb"`
				} `json:"properties"`
			}
			var eachCallidData RetrieveDashboardOut

			// make a http get request to each xcallid
			var url string = "http://staging.vishwamcorp.com/v2/dashboard/"

			resp, err := http.Get(url + callid)
			if err != nil {
				fmt.Println(err)
			} else {

				body_values, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Fatal(err)
				}
				err2 := json.Unmarshal([]byte(body_values), &eachCallidData)
				if err != nil {
					fmt.Println(err2)
				}
				//if eachCallidData.AppID == "jiophone" {
				//	jio_transactions += 1
				//	jio_users_repeated = append(jio_users_repeated, eachCallidData.UserID)
				//
				//}
				//if eachCallidData.Properties.StatusCode == 200 {
				//
				//	total200 = total200 + 1
				//
				//}
				//if eachCallidData.Properties.StatusCode == 200 && eachCallidData.Properties.AutoResp == 404 {
				//
				//	frFailures = frFailures + 1
				//}
				if(eachCallidData.AppID == "vishwam"){
					fmt.Println(eachCallidData.UserID)

					uns1 := strings.Split(eachCallidData.UserID,"_")
					if(len(uns1[0])==8){
						tp := uns1[0]
						if(tp[:5] == "55031"){
							bondUsers = append(bondUsers, uns1[0])
						}

					}else if(len(uns1[0])<8){
						tp := uns1[0]
						if(tp != "mum" || tp != "hyd" || len(tp) != 3){
							last3 := tp[len(tp)-3:]
							username := "55031"+last3
							bondUsers = append(bondUsers,username)
						}
						//tp := uns1[0]


					}else if(len(uns1[0])>8){
						tp:= uns1[0]
						username := tp[:8]
						bondUsers = append(bondUsers,username)

					}else if(len(uns1[0])==9){
						tp := uns1[0]
						bondUsers = append(bondUsers, tp)


					}





				}



			}

		}



	}

	fmt.Println("Total valid bond transactions:",len(bondUsers))
	uniqueBondusers := getUniqueUserIds(bondUsers)

	var usertrans = [][]string{{"username","transcount"}}

	for i,_:= range uniqueBondusers{
		//var userrow []string
		userrow := []string{uniqueBondusers[i].Un,strconv.Itoa(uniqueBondusers[i].Count)}

		usertrans = append(usertrans,userrow)
	}

	rand.Seed(time.Now().UnixNano())
	ran2 := strconv.Itoa(rand.Int())
	//reportname2 := "usertrans_bondIds2_withoutconditions.csv"
	reportname2:= "usertrans_individual_"+date+"_"+ran2+".csv"
	file2, err2 := os.Create( "public/csv_reports/" +reportname2)
	checkError("Cannot create file", err2)
	defer file2.Close()

	writer2 := csv.NewWriter(file2)
	defer writer2.Flush()

	for index, _ := range usertrans {
		err := writer2.Write(usertrans[index])
		checkError("Cannot write to file", err)
	}


	fmt.Println("report for individual transactions details is generated.")


}
func getTransactionsReport(date string) {

	var keyList []string

	var path string = "reliance_testing/" + date

	image_keylist := getListObject(path)

	//var last_key string
	for _, keys := range image_keylist.Contents {

		// fmt.Println('$')
		keyList = append(keyList, *keys.Key)

	}

	if *image_keylist.IsTruncated == true {

		subKeyList := getTruncatedKeys(*image_keylist.IsTruncated, *image_keylist.Contents[*image_keylist.MaxKeys-1].Key, date)
		for _, kkkk := range subKeyList {

			// fmt.Print("@")
			keyList = append(keyList, kkkk)
		}
	}

	var jio_transactions int = 0
	var jio_users_repeated []string
	uci := getUniqueXcallids(keyList)
	//users := getUniqueUserIds(keyList)
	// var count500 int
	// var count424 int
	// var count406 int
	// var count410 int
	var frFailures int = 0
	var total200 int = 0
	for i, _ := range uci {

		if uci[i].CI != "NA" {

			var callid = uci[i].CI
			// fmt.Println(callid)
			type RetrieveDashboardOut struct {
				ID         string `json:"id,omitempty"`
				UserID     string `json:"userID,omitempty"`
				AppID      string `json:"appID,omitempty"`
				Path       string `json:"path,omitempty"`
				Env        string `json:"env,omitempty"`
				Properties struct {
					Path       string `json:"path,omitempty"`
					NewUser    int    `json:"newuser,omitempty"`
					StatusCode int    `json:"statusCode,omitempty"`
					// StatusCodeDB     int           `json:"statusCodeDB,omitempty"`
					AutoResp         int           `json:"autoResp,omitempty"`
					Error            string        `json:"error,omitempty"`
					FaceRatio        string        `json:"faceratio,omitempty"`
					FaceSpan         time.Duration `json:"facespan,omitempty"`
					SpoofSpan        time.Duration `json:"spoofspan,omitempty"`
					ProbabilitySpan  time.Duration `json:"probabilityspan,omitempty"`
					PeripheralSpan   time.Duration `json:"PeripheralSpan,omitempty"`
					Latency          int           `json:"latency,omitempty"`
					StartTime        time.Time     `json:"starttime,omitempty"`
					EndTime          time.Time     `json:"endtime,omitempty"`
					Version          int           `json:"version,omitempty"`
					Distance         float64       `json:"distance"`
					TypeFrom         string        `json:"typefrom"`
					Probabilities    []float64     `json:"probabilities"`
					ProbMean         float64       `json:"probMean"`
					LiveMean         float64       `json:"liveMean"`
					RefMean          float64       `json:"refMean"`
					Blur             float64       `json:"blur"`
					ProbabilitiesRef []float64     `json:"probabilitiesref"`
					Landmarks        string        `json:"landmarks"`
					LandmarksDb      [][]int       `json:"landmarksdb"`
				} `json:"properties"`
			}
			var eachCallidData RetrieveDashboardOut

			// make a http get request to each xcallid
			var url string = "http://staging.vishwamcorp.com/v2/dashboard/"

			resp, err := http.Get(url + callid)
			if err != nil {
				fmt.Println(err)
			} else {

				body_values, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Fatal(err)
				}
				err2 := json.Unmarshal([]byte(body_values), &eachCallidData)
				if err != nil {
					fmt.Println(err2)
				}
				if eachCallidData.AppID == "jiophone" {
					jio_transactions += 1
					jio_users_repeated = append(jio_users_repeated, eachCallidData.UserID)

				}
				if eachCallidData.Properties.StatusCode == 200 {

					total200 = total200 + 1

				}
				if eachCallidData.Properties.StatusCode == 200 && eachCallidData.Properties.AutoResp == 404 {

					frFailures = frFailures + 1
				}

			}

		}

	}

	fmt.Println("Total Number of Transactionss:", len(uci))
	//fmt.Println("Total Number of Unique Used Id's:", len(users))
	jio_unique_users := getUniqueJioUsers(jio_users_repeated)
	fmt.Println("Total Jio Transactions:", jio_transactions)
	fmt.Println("Total Unique Jio Users:", len(jio_unique_users), len(jio_users_repeated))
	fmt.Println("Total 200 transactions:", total200)
	fmt.Println("Total fr failures:", frFailures)

	// fmt.Println("Total Unique Jio Users:", len()

}
func getUniqueJioUsers(list []string) []users {
	var jioUsers []users
	var count int = 0
	for _, item := range list {

		if isUserIdCaptured(jioUsers, item) == false {
			var tmp users
			tmp.Un = item
			tmp.Count = 1
			jioUsers = append(jioUsers, tmp)
			count += 1
		}

		//fmt.Println("")
	}
	fmt.Println("No of Unique jio Users:", count)
	return jioUsers

}

func downloadImages(date string) {

	_ = os.Mkdir(date, 0777)
	_ = os.MkdirAll(date+"/200_crop", 0777)
	_ = os.MkdirAll(date+"/200_full", 0777)

	_ = os.MkdirAll(date+"/406_crop", 0777)
	_ = os.MkdirAll(date+"/406_full", 0777)
	_ = os.MkdirAll(date+"/404_crop", 0777)
	_ = os.MkdirAll(date+"/404_full", 0777)
	var real_crop string = date + "/200_crop/"
	var real_full string = date + "/200_full/"
	var fake_crop string = date + "/406_crop/"
	var fake_full string = date + "/406_full/"
	var frfail_crop string = date + "/404_crop/"
	var frfail_full string = date + "/404_full/"

	var keyList []string
	// date := "2018-11-27"
	var path string = "reliance_testing/" + date

	image_keylist := getListObject(path)

	//var last_key string
	for _, keys := range image_keylist.Contents {

		fmt.Println(*keys.Key)
		keyList = append(keyList, *keys.Key)

	}

	if *image_keylist.IsTruncated == true {
		subKeyList := getTruncatedKeys(*image_keylist.IsTruncated, *image_keylist.Contents[*image_keylist.MaxKeys-1].Key, date)
		for _, kkkk := range subKeyList {

			fmt.Println(kkkk)
			keyList = append(keyList, kkkk)
		}

	}

	// TO donwload real,fake images in seperate folders

	fmt.Println(len(keyList))

	for _, keyList := range keyList {

		var respcode_man string
		var file_ext string
		fn := getFileName(keyList)
		fmt.Println("test", fn)
		filesplits := strings.Split(fn, "_")
		if len(filesplits) != 2 {
			respcode_man = filesplits[1]
			file_ext = filesplits[3]

		}

		if respcode_man == "200" && file_ext == "image.png" {
			downloadImage(real_full+fn, keyList)
		} else if respcode_man == "200" && file_ext == "image2.png" {
			downloadImage(real_crop+fn, keyList)
		} else if respcode_man == "406" && file_ext == "image.png" {
			downloadImage(fake_full+fn, keyList)
		} else if respcode_man == "406" && file_ext == "image2.png" {
			downloadImage(fake_crop+fn, keyList)
		} else if respcode_man == "404" && file_ext == "image.png" {
			downloadImage(frfail_full+fn, keyList)
		} else if respcode_man == "404" && file_ext == "image2.png" {
			downloadImage(frfail_crop+fn, keyList)
		}

	}

	//To dowload all images in one folder

	// folder := "public/download_images"
	// for _, keyList := range keyList {
	// 	fn := getFileName(keyList)
	// 	downloadImage(folder+fn, keyList)

	// }

}

func saveImage(imagePath string, imageUrl string) {

	img, err := http.Get(imageUrl)
	if err != nil {
		fmt.Println(err)
	}

	defer img.Body.Close()

	file, err := os.Create(imagePath)
	if err != nil {
		exitErrorf("Unable to open file %q, %v", err)
	}

	defer file.Close()

	_, err = io.Copy(file, img.Body)

}
func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
func getContext() context.Context {
	ctx := context.Background()
	return ctx
}

func getprobString(probabilities []float64) (string, float64) {
	var prob string = "["
	var mean_score float64 = 0.00
	for x, _ := range probabilities {
		//fmt.Println(probabilities[x])
		if x == 0 {
			prob = prob + strconv.FormatFloat(probabilities[x], 'g', 4, 64)
		} else {
			prob = prob + ", "
			prob = prob + strconv.FormatFloat(probabilities[x], 'g', 4, 64)
		}

		mean_score = mean_score + (float64((x + 1)) * (probabilities[x]))
	}

	prob = prob + " ]"

	return prob, mean_score

}
func getgoxcallidClient() *db.Client {

	conf := &firebase.Config{
		DatabaseURL: "https://go-xcallid.firebaseio.com/",
	}

	opt := option.WithCredentialsFile("go_xcallid_credentials.json")
	app, err := firebase.NewApp(getContext(), conf, opt)
	if err != nil {

		fmt.Errorf("error initializing app: %v", err)

	}

	client, err := app.Database(getContext())
	if err != nil {
		log.Fatalln("Error initializing database client:", err)
	}

	return client
}
func getAllImagesList(date string) {

	// var real_path string = "public/download_images/26_nov_real_fake_frfailures/real_images/"
	// var fake_path string = "public/download_images/26_nov_real_fake_frfailures/fake_images/"
	// var frfail_path string = "public/download_images/26_nov_real_fake_frfailures/fr_failures/"
	// param := r.URL.Query()
	// truncKeyList = []string
	var tracesData = [][]string{{"Serial", "Start Time", "End_Time", "XCallId", "UserID", "App_ID", "Path", "Reference_Image", "Reference_Cropped_Image", "New_Ref_image", "Auth_Image", "Auth_Cropped_Image", "Auth_crop_resolution", "Resolution Ratio", "Deep Check Response", "Automatic Response", "Response Time(ms)", "Probabilities", "Probabilities_Ref", "Prob_Mean", "Live_Mean", "Live_Mean2", "Live_Mean3", "Live_Mean4", "Live_Mean3_30E", "livemean hr", "livemean mac", "livemean ppr", "livemean ppr2", "livemean scr", "Ref_Mean", "UniMean","Person Prob","Screen Prob","Print prob","fiop prob","liveOBD","Distance", "Blurriness", "Landmarks", "Face Span", "Spoof Span", "Spoof Span2", "Spoof Span3", "Spoof Span 4", "Spoof Span3_30E", "Spoof Span Hr", "Spoof Span Mac", "Spoof Span Scr", "Spoof Span Paper", "Probability Span","Spooffiopspan", "User Feedback", "Peripheral Span", "Type From"}}
	var keyList []string
	var userIDs []string

	_ = os.MkdirAll("transactions/"+date, 0777)
	var path string = "reliance_testing/" + date

	image_keylist := getListObject(path)

	//var last_key string
	for _, keys := range image_keylist.Contents {

		fmt.Println(*keys.Key)
		keyList = append(keyList, *keys.Key)

	}

	if *image_keylist.IsTruncated == true {

		subKeyList := getTruncatedKeys(*image_keylist.IsTruncated, *image_keylist.Contents[*image_keylist.MaxKeys-1].Key, date)
		for _, kkkk := range subKeyList {

			fmt.Println(kkkk)
			keyList = append(keyList, kkkk)
		}
	}

	fmt.Println(len(keyList))

	type callidFeedBack struct {
		AppID    string `json:"appID,omitempty"`
		Comment  string `json:"comment,omitempty"`
		Decision string `json:"decision,omitempty"`
		Time     string `json:"time,omitempty"`
		XcallID  string `json:"x_callID,omitempty"`
	}

	var feedBackData map[string]callidFeedBack
	var jio_transactions int = 0
	var jio_users_repeated []string

	ref2 := getgoxcallidClient().NewRef("/FeedBack-staging")
	error2 := ref2.Get(getContext(), &feedBackData)
	if error2 != nil {
		log.Fatal(error2)
		// fmt.Println(error2)
		// fmt.Println(feedBackData)

	}
	//fmt.Println(feedBackData)
	uci := getUniqueXcallids(keyList)
	//users := getUniqueUserIds(keyList)
	//fmt.Println("No Of Unique user_id's:", len(users))
	//fmt.Println("ALL USERS:")
	//for _, user := range users {
	//	fmt.Println(user.Un,user.Count)
	//}



	fmt.Println("No Of Unique Xcallid's:", len(uci))
	//fmt.Println("ALL XCALLIDS:")
	//for _, u := range uci {
	//	fmt.Println(u.CI)
	//}

	for i, _ := range uci {

		if uci[i].CI != "NA" {
			fmt.Println(i, uci[i].CI)

			var feedback_json callidFeedBack
			feedback_json = feedBackData[uci[i].CI]

			fmt.Println("callid_feedback_json", feedback_json.Decision)

			var callid = uci[i].CI
			// fmt.Println(callid)
			// _ = os.MkdirAll("transactions/"+date+callid, 0777)
			type RetrieveDashboardOut struct {
				ID         string `json:"id,omitempty"`
				UserID     string `json:"userID,omitempty"`
				AppID      string `json:"appID,omitempty"`
				Path       string `json:"path,omitempty"`
				Env        string `json:"env,omitempty"`
				Properties struct {
					Path       string `json:"path,omitempty"`
					NewUser    int    `json:"newuser,omitempty"`
					StatusCode int    `json:"statusCode,omitempty"`
					// StatusCodeDB     int           `json:"statusCodeDB,omitempty"`
					AutoResp        int       `json:"autoResp,omitempty"`
					Error           string    `json:"error,omitempty"`
					FaceRatio       string    `json:"faceratio,omitempty"`
					FaceSpan        string    `json:"facespan,omitempty"`
					ObjDetSpan      string    `json:"objDetSpan,omitempty"`
					SpoofspanPpr    string    `json:"spoofspanPpr,omitempty"`
					SpoofSpanHr     string    `json:"spoofspanHr,omitempty"`
					SpoofSpanMac    string    `json:"spoofspanMac,omitempty"`
					SpoofspanScr    string    `json:"spoofspanScr,omitempty"`
					SpoofSpan       string    `json:"spoofspan,omitempty"`
					SpoofSpan2      string    `json:"spoofspan2,omitempty"`
					SpoofSpan3      string    `json:"spoofspan3,omitempty"`
					SpoofSpan4      string    `json:"spoofspan4,omitempty"`
					SpoofSpan3_30E  string    `json:"spoofspan3_30E,omitempty"`
					SpoofFiopSpan   string    `jsno:"Spooffiopspan,omitempty"`
					ProbabilitySpan string    `json:"probabilityspan,omitempty"`
					PeripheralSpan  string    `json:"PeripheralSpan,omitempty"`
					Latency         int       `json:"latency,omitempty"`
					StartTime       time.Time `json:"starttime,omitempty"`
					EndTime         time.Time `json:"endtime,omitempty"`
					Version         int       `json:"version,omitempty"`
					Distance        float64   `json:"distance"`
					TypeFrom        string    `json:"typefrom"`
					Probabilities   []float64 `json:"probabilities"`
					ProbMean        float64   `json:"probMean,omitempty"`
					LiveMean        float64   `json:"liveMean,omitempty"`
					LiveMean2       float64   `json:"liveMean2,omitempty"`
					LiveMean3       float64   `json:"liveMean3,omitempty"`
					LiveMean4       float64   `json:"liveMean4,omitempty"`
					LiveMean3_30E   float64   `json:"liveMean3_30E,omitempty"`
					LiveMeanHr      float64   `json:"liveMeanHr,omitempty"`
					LiveMeanMac     float64   `json:"liveMeanMac,omitempty"`
					LiveMeanPpr     float64   `json:"liveMeanPpr,omitempty"`
					LiveMeanPpr2    float64   `json:"liveMeanPpr2,omitempty"`
					LiveMeanScr     float64   `json:"liveMeanScr,omitempty"`
					UniMean	        float64   `json:"uniMean,omitempty"`
					PersonProb      float64   `json:"personProb,omitempty"`
					ScreenProb      float64   `json:"screeenProb,omitempty"`
					PrintProb       float64   `json:"printProb,omitempty"`
					FiopMean        float64   `json:"fiopMean,omitempty"`
					LiveOBD         string   `json:"liveOBD,omitempty"`
					// LiveMeanMob      float64   `json:"liveMeanMob"`
					RefMean          float64   `json:"refMean"`
					Blur             float64   `json:"blur"`
					ProbabilitiesRef []float64 `json:"probabilitiesref"`
					Landmarks        string    `json:"landmarks"`
					LandmarksDb      [][]int   `json:"landmarksdb"`
				} `json:"properties"`
			}
			var eachCallidData RetrieveDashboardOut
			var refpath string
			var ref_crop string
			var new_ref string
			var image1 string
			var image2 string
			var probabilities string
			var probabilities_ref string
			var prob_mean string
			var live_mean string
			var live_mean2 string
			var live_mean3 string
			var live_mean4 string
			var ref_mean string
			var distance string
			var resp_time string
			var resolution_ratio string
			var sTime string
			var eTime string
			var authCropResolution string
			var autoresp string
			var blur string
			var landmarks string
			var livemeanPpr string
			var livemeanScr string
			var livemeanHr string
			var livemean3_30E string
			var livemeanMac string
			var livemeanPpr2 string
			var unimean string
			var personProb string
			var screenProb string
			var printProb string
			var fiopMean string
			var liveOBD string


			// var livemeanMob string
			var user_feedback string = feedback_json.Decision
			// make a http get request to each xcallid
			var url string = "https://staging.vishwamcorp.com/v2/dashboard/"

			resp, err := http.Get(url + callid)
			if err != nil {
				fmt.Println(err)
			} else {

				body_values, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Fatal(err)
				}
				err2 := json.Unmarshal([]byte(body_values), &eachCallidData)
				if err != nil {
					fmt.Println(err2)
				}
				if eachCallidData.AppID == "jiophone" {
					jio_transactions += 1
					jio_users_repeated = append(jio_users_repeated, eachCallidData.UserID)

				}

				properties := eachCallidData.Properties
				//fmt.Println(resp.Body, resp.StatusCode)

				responsecode := properties.StatusCode

				userIDs = append(userIDs,eachCallidData.UserID)
				// autoresp = strconv.Itoa(properties.autoResp)
				fmt.Println(responsecode)
				fmt.Println(eachCallidData.UserID, eachCallidData.Path)
				if eachCallidData.Path == "/v2/me/reference_ios" || eachCallidData.Path == "/v1/me/reference" {
					refpath = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image.png"
					ref_crop = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/ref_crop.png"
					new_ref = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/ref:" + eachCallidData.UserID
					image1 = ""
					image2 = ""
					ps, pm := getprobString(properties.Probabilities)
					probabilities = ps
					probabilities_ref = ""
					prob_mean = strconv.FormatFloat(pm, 'g', 6, 64)
					live_mean = strconv.FormatFloat(properties.LiveMean, 'g', 6, 64)
					ref_mean = strconv.FormatFloat(properties.RefMean, 'g', 6, 64)
					distance = ""
					resp_time = strconv.Itoa(properties.Latency)
					resolution_ratio = ""
					loc, _ := time.LoadLocation("Asia/Kolkata")
					startTime := properties.StartTime
					fmt.Println("UTC time:", startTime)
					sTime = startTime.In(loc).String()
					fmt.Println("start time:", sTime)
					endTime := properties.EndTime
					eTime = endTime.In(loc).String()
					authCropResolution = ""
					autoresp = strconv.Itoa(properties.AutoResp)
					blur = strconv.FormatFloat(properties.Blur, 'g', 6, 64)
					landmarks = ""
					// livemeanMob = ""
					livemeanPpr = ""
					livemeanPpr2 = ""
					livemeanScr = ""
					live_mean2 = ""
					live_mean3 = ""
					live_mean4 = ""
					livemeanHr = ""
					livemeanMac = ""
					livemean3_30E = ""
					unimean = ""
					personProb = ""
					printProb = ""
					screenProb = ""
					fiopMean = ""
					liveOBD = ""

				} else if eachCallidData.Path == "/v2/single_gesture_ios" || eachCallidData.Path == "/v1/single_gesture" {
					refpath = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/reference_image.png"
					image1 = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image.png"
					image2 = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image2.png"
					// full_resolution = ""
					// cropped_resolution = ""

					// fmt.Println(authCropResolution)
					authCropResolution = ""

					// if cropped_resolution == 0 || full_resolution == 0 {
					// 	resolution_ratio = "No images found"
					// } else {
					// 	fmt.Println("resolution:", full_resolution, cropped_resolution)
					// 	resolution_ratio = strconv.FormatFloat(float64(full_resolution)/float64(cropped_resolution), 'g', 6, 64)
					// }
					resolution_ratio = properties.FaceRatio
					// if responsecode == 200 {
					// 	_ = os.MkdirAll("transactions/"+date+"/"+callid, 0777)
					// 	refFileName := "ref_full.png"
					// 	saveImage("transactions/"+date+"/"+callid+"/"+refFileName, refpath)
					// 	authFileName := "auth_full.png"
					// 	saveImage("transactions/"+date+"/"+callid+"/"+authFileName, image1)
					// }

					ref_crop = "https://staging.vishwamcorp.com/v2/traces/" + callid + "/ref_crop.png"
					new_ref = "https://staging.vishwamcorp.com/v2/traces/" + callid + "/ref:" + eachCallidData.UserID
					ps, pm := getprobString(properties.Probabilities)
					probabilities = ps
					psr, pmr := getprobString(properties.ProbabilitiesRef)
					probabilities_ref = psr
					prob_mean = strconv.FormatFloat(pm, 'g', 6, 64)
					live_mean = strconv.FormatFloat(properties.LiveMean, 'g', 6, 64)
					ref_mean = strconv.FormatFloat(pmr, 'g', 6, 64)
					distance = strconv.FormatFloat(properties.Distance, 'g', 6, 64)
					resp_time = strconv.Itoa(properties.Latency)
					loc, _ := time.LoadLocation("Asia/Kolkata")
					startTime := properties.StartTime
					fmt.Println("UTC time:", startTime)
					sTime = startTime.In(loc).String()
					fmt.Println("start time:", sTime)
					endTime := properties.EndTime
					eTime = endTime.In(loc).String()

					autoresp = strconv.Itoa(properties.AutoResp)
					blur = strconv.FormatFloat(properties.Blur, 'g', 6, 64)
					landmarks = properties.Landmarks
					// livemeanMob = strconv.FormatFloat(properties., 'g', 6, 64)
					// fmt.Println(properties)
					livemeanPpr = strconv.FormatFloat(properties.LiveMeanPpr, 'g', 6, 64)
					livemeanPpr2 = strconv.FormatFloat(properties.LiveMeanPpr2, 'g', 6, 64)
					livemeanScr = strconv.FormatFloat(properties.LiveMeanScr, 'g', 6, 64)
					live_mean2 = strconv.FormatFloat(properties.LiveMean2, 'g', 6, 64)
					live_mean3 = strconv.FormatFloat(properties.LiveMean3, 'g', 6, 64)
					live_mean4 = strconv.FormatFloat(properties.LiveMean4, 'g', 6, 64)
					livemeanHr = strconv.FormatFloat(properties.LiveMeanHr, 'g', 6, 64)
					livemeanMac = strconv.FormatFloat(properties.LiveMeanMac, 'g', 6, 64)
					livemean3_30E = strconv.FormatFloat(properties.LiveMean3_30E, 'g', 6, 64)
					unimean =  strconv.FormatFloat(properties.UniMean, 'g', 6, 64)
					personProb =  strconv.FormatFloat(properties.PersonProb, 'g', 6, 64)
					screenProb =  strconv.FormatFloat(properties.ScreenProb, 'g', 6, 64)
					printProb =  strconv.FormatFloat(properties.PrintProb, 'g', 6, 64)
					fiopMean = strconv.FormatFloat(properties.FiopMean, 'g', 6, 64)
					liveOBD = properties.LiveOBD
				}

				eachCID := []string{strconv.Itoa(i + 1), sTime, eTime, callid, eachCallidData.UserID, eachCallidData.AppID, eachCallidData.Path, refpath, ref_crop, new_ref, image1, image2, authCropResolution, resolution_ratio, strconv.Itoa(responsecode), autoresp, resp_time, probabilities, probabilities_ref, prob_mean, live_mean, live_mean2, live_mean3, live_mean4, livemean3_30E, livemeanHr, livemeanMac, livemeanPpr, livemeanPpr2, livemeanScr, ref_mean,unimean,personProb,screenProb,printProb,fiopMean,liveOBD, distance, blur, landmarks, properties.FaceSpan, properties.SpoofSpan, properties.SpoofSpan2, properties.SpoofSpan3, properties.SpoofSpan4, properties.SpoofSpan3_30E, properties.SpoofSpanHr, properties.SpoofSpanMac, properties.SpoofspanScr, properties.SpoofspanPpr, properties.ProbabilitySpan, properties.SpoofFiopSpan, user_feedback, properties.PeripheralSpan, properties.TypeFrom}

				//fmt.Println(eachCID)
				//resp := hitApi(ref_crop,image2,eachCallidData.UserID)
				//fmt.Println(resp)
				tracesData = append(tracesData, eachCID)

			}

		}

	}
	fmt.Println("No Of Unique Xcallid's:", len(uci))
	fmt.Println("ALL XCALLIDS:")
	//fmt.Println("No Of Unique user_id's:", len(usersd))
	fmt.Println("ALL USERS:")
	jio_unique_users := getUniqueJioUsers(jio_users_repeated)
	fmt.Println("Total Jio Transactions:", jio_transactions)
	fmt.Println("Total Unique Jio Users:", len(jio_unique_users), len(jio_users_repeated))
	for _, juser := range jio_unique_users {
		fmt.Println(juser)
	}
	rand.Seed(time.Now().UnixNano())
	ran := strconv.Itoa(rand.Int())
	reportname := date + "_1_" + ran + ".csv"
	file, err := os.Create( "public/csv_reports/" +reportname)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for index, _ := range tracesData {
		err := writer.Write(tracesData[index])
		checkError("Cannot write to file", err)
	}

	fmt.Println("ALL USERS:")
	users := getUniqueUserIds(userIDs)

	for _, user := range users {
		fmt.Println(user.Un,user.Count)
	}

	var usertrans = [][]string{{"username","transcount"}}

	for i,_:= range users{
		//var userrow []string
		userrow := []string{users[i].Un,strconv.Itoa(users[i].Count)}

		usertrans = append(usertrans,userrow)
	}

	rand.Seed(time.Now().UnixNano())
	//ran2 := strconv.Itoa(rand.Int())
	reportname2 := "usertrans"
	file2, err2 := os.Create( "public/csv_reports/" +reportname2)
	checkError("Cannot create file", err2)
	defer file2.Close()

	writer2 := csv.NewWriter(file2)
	defer writer2.Flush()

	for index, _ := range usertrans {
		err := writer2.Write(usertrans[index])
		checkError("Cannot write to file", err)
	}



}

func getAll(date string) {

	// var real_path string = "public/download_images/26_nov_real_fake_frfailures/real_images/"
	// var fake_path string = "public/download_images/26_nov_real_fake_frfailures/fake_images/"
	// var frfail_path string = "public/download_images/26_nov_real_fake_frfailures/fr_failures/"
	// param := r.URL.Query()
	// truncKeyList = []string
	var tracesData = [][]string{{"Serial", "XCallID", "App_ID", "path", "latency", "Request Time", "Peripheral Span"}}
	var keyList []string

	_ = os.MkdirAll("transactions/"+date, 0777)
	var path string = "reliance_testing/" + date

	image_keylist := getListObject(path)

	//var last_key string
	for _, keys := range image_keylist.Contents {

		fmt.Println(*keys.Key)
		keyList = append(keyList, *keys.Key)

	}

	if *image_keylist.IsTruncated == true {

		subKeyList := getTruncatedKeys(*image_keylist.IsTruncated, *image_keylist.Contents[*image_keylist.MaxKeys-1].Key, date)
		for _, kkkk := range subKeyList {

			fmt.Println(kkkk)
			keyList = append(keyList, kkkk)
		}
	}

	fmt.Println(len(keyList))

	// type callidFeedBack struct {
	// 	AppID    string `json:"appID,omitempty"`
	// 	Comment  string `json:"comment,omitempty"`
	// 	Decision string `json:"decision,omitempty"`
	// 	Time     string `json:"time,omitempty"`
	// 	XcallID  string `json:"x_callID,omitempty"`
	// }

	// var feedBackData map[string]callidFeedBack
	var jio_transactions int = 0
	var jio_users_repeated []string

	// ref2 := getgoxcallidClient().NewRef("/FeedBack-staging")
	// error2 := ref2.Get(getContext(), &feedBackData)
	// if error2 != nil {
	// 	log.Fatal(error2)
	// 	// fmt.Println(error2)
	// 	// fmt.Println(feedBackData)

	// }
	// fmt.Println(feedBackData)
	uci := getUniqueXcallids(keyList)
	users := getUniqueUserIds(keyList)
	fmt.Println("No Of Unique user_id's:", len(users))
	fmt.Println("ALL USERS:")
	for _, user := range users {
		fmt.Println(user)
	}

	fmt.Println("No Of Unique Xcallid's:", len(uci))
	fmt.Println("ALL XCALLIDS:")
	for _, u := range uci {
		fmt.Println(u.CI)
	}

	for i, _ := range uci {

		if uci[i].CI != "NA" {
			fmt.Println(i, uci[i].CI)

			// var feedback_json callidFeedBack
			// feedback_json = feedBackData[uci[i].CI]

			// fmt.Println("callid_feedback_json", feedback_json.Decision)

			var callid = uci[i].CI
			// fmt.Println(callid)
			// _ = os.MkdirAll("transactions/"+date+callid, 0777)
			type RetrieveDashboardOut struct {
				ID         string `json:"id,omitempty"`
				UserID     string `json:"userID,omitempty"`
				AppID      string `json:"appID,omitempty"`
				Path       string `json:"path,omitempty"`
				Env        string `json:"env,omitempty"`
				Properties struct {
					Path       string `json:"path,omitempty"`
					NewUser    int    `json:"newuser,omitempty"`
					StatusCode int    `json:"statusCode,omitempty"`
					// StatusCodeDB     int           `json:"statusCodeDB,omitempty"`
					AutoResp        int       `json:"autoResp,omitempty"`
					Error           string    `json:"error,omitempty"`
					FaceRatio       string    `json:"faceratio,omitempty"`
					FaceSpan        string    `json:"facespan,omitempty"`
					ObjDetSpan      string    `json:"objDetSpan,omitempty"`
					SpoofspanPpr    string    `json:"spoofspanPpr,omitempty"`
					SpoofSpanHr     string    `json:"spoofspanHr,omitempty"`
					SpoofSpanMac    string    `json:"spoofspanMac,omitempty"`
					SpoofspanScr    string    `json:"spoofspanScr,omitempty"`
					SpoofSpan       string    `json:"spoofspan,omitempty"`
					SpoofSpan2      string    `json:"spoofspan2,omitempty"`
					SpoofSpan3      string    `json:"spoofspan3,omitempty"`
					SpoofSpan4      string    `json:"spoofspan4,omitempty"`
					SpoofSpan3_30E  string    `json:"spoofspan3_30E,omitempty"`
					ProbabilitySpan string    `json:"probabilityspan,omitempty"`
					PeripheralSpan  string    `json:"PeripheralSpan,omitempty"`
					Latency         int       `json:"latency,omitempty"`
					StartTime       time.Time `json:"starttime,omitempty"`
					EndTime         time.Time `json:"endtime,omitempty"`
					Version         int       `json:"version,omitempty"`
					Distance        float64   `json:"distance"`
					TypeFrom        string    `json:"typefrom"`
					Probabilities   []float64 `json:"probabilities"`
					ProbMean        float64   `json:"probMean,omitempty"`
					LiveMean        float64   `json:"liveMean,omitempty"`
					LiveMean2       float64   `json:"liveMean2,omitempty"`
					LiveMean3       float64   `json:"liveMean3,omitempty"`
					LiveMean4       float64   `json:"liveMean4,omitempty"`
					LiveMean3_30E   float64   `json:"liveMean3_30E,omitempty"`
					LiveMeanHr      float64   `json:"liveMeanHr,omitempty"`
					LiveMeanMac     float64   `json:"liveMeanMac,omitempty"`
					LiveMeanPpr     float64   `json:"liveMeanPpr,omitempty"`
					LiveMeanPpr2    float64   `json:"liveMeanPpr2,omitempty"`
					LiveMeanScr     float64   `json:"liveMeanScr,omitempty"`
					// LiveMeanMob      float64   `json:"liveMeanMob"`
					RefMean          float64   `json:"refMean"`
					Blur             float64   `json:"blur"`
					ProbabilitiesRef []float64 `json:"probabilitiesref"`
					Landmarks        string    `json:"landmarks"`
					LandmarksDb      [][]int   `json:"landmarksdb"`
				} `json:"properties"`
			}

			var resp_time string
			var reqTime string
			var eachCallidData RetrieveDashboardOut
			// var peripheralSpan string

			// var livemeanMob string
			// var user_feedback string = ""

			// make a http get request to each xcallid
			var url string = "https://staging.vishwamcorp.com/v2/dashboard/"

			resp, err := http.Get(url + callid)
			if err != nil {
				fmt.Println(err)
			} else {

				body_values, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Fatal(err)
				}
				err2 := json.Unmarshal([]byte(body_values), &eachCallidData)
				if err != nil {
					fmt.Println(err2)
				}
				if eachCallidData.AppID == "jiophone" {
					jio_transactions += 1
					jio_users_repeated = append(jio_users_repeated, eachCallidData.UserID)

				}

				properties := eachCallidData.Properties
				//fmt.Println(resp.Body, resp.StatusCode)

				// responsecode := properties.StatusCode
				// autoresp = strconv.Itoa(properties.autoResp)
				// fmt.Println(responsecode)
				var eachCID []string
				fmt.Println(eachCallidData.UserID, eachCallidData.Path)

				resp_time = strconv.Itoa(properties.Latency)
				loc, _ := time.LoadLocation("Asia/Kolkata")
				startTime := properties.StartTime
				reqTime = startTime.In(loc).String()
				// fmt.Println("start time:", sTime)
				tempData := []string{strconv.Itoa(i + 1), callid, eachCallidData.AppID, eachCallidData.Path, resp_time, reqTime, properties.PeripheralSpan}
				eachCID = tempData

				//fmt.Println(eachCID)
				//resp := hitApi(ref_crop,image2,eachCallidData.UserID)
				//fmt.Println(resp)
				tracesData = append(tracesData, eachCID)

			}

		}

	}
	fmt.Println("No Of Unique Xcallid's:", len(uci))
	fmt.Println("ALL XCALLIDS:")
	fmt.Println("No Of Unique user_id's:", len(users))
	fmt.Println("ALL USERS:")
	jio_unique_users := getUniqueJioUsers(jio_users_repeated)
	fmt.Println("Total Jio Transactions:", jio_transactions)
	fmt.Println("Total Unique Jio Users:", len(jio_unique_users), len(jio_users_repeated))
	for _, juser := range jio_unique_users {
		fmt.Println(juser)

	}
	rand.Seed(time.Now().UnixNano())
	ran := strconv.Itoa(rand.Int())
	reportname := date + "_1_" + ran + ".csv"
	file, err := os.Create("public/csv_reports/" + reportname)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for index, _ := range tracesData {
		err := writer.Write(tracesData[index])
		checkError("Cannot write to file", err)
	}

}

func generateModelsReport(){

	var tracesData = [][]string{{"Serial", "Start Time", "End_Time", "XCallId", "UserID", "App_ID", "Path", "Reference_Image", "Reference_Cropped_Image", "New_Ref_image", "Auth_Image", "Auth_Cropped_Image", "Auth_crop_resolution", "Resolution Ratio", "Deep Check Response", "Automatic Response", "Response Time(ms)", "Probabilities", "Probabilities_Ref", "Prob_Mean", "Live_Mean", "Live_Mean2", "Live_Mean3", "Live_Mean4", "Live_Mean3_30E", "livemean hr", "livemean mac", "livemean ppr", "livemean ppr2", "livemean scr", "Ref_Mean", "UniMean","Person Prob","Screen Prob","Print prob","fiop prob","liveOBD","Distance", "Blurriness", "Landmarks", "Face Span", "Spoof Span", "Spoof Span2", "Spoof Span3", "Spoof Span 4", "Spoof Span3_30E", "Spoof Span Hr", "Spoof Span Mac", "Spoof Span Scr", "Spoof Span Paper", "Probability Span","Spooffiopspan", "User Feedback", "Peripheral Span", "Type From"}}
	//var keyList []string
	//var userIDs []string


	fmt.Println("started reading csv file")

	var callIDs []string
	r := easycsv.NewReaderFile("public/csv_reports/new.csv")
	var change struct {
		New string `index:"0"`
	}

	for r.Read(&change) {

		fmt.Println(change.New)
		//csvRow := []string{change.Old, cange.New}

		callIDs = append(callIDs, change.New)

	}

	if err := r.Done(); err != nil {
		log.Fatalf("Failed to read a CSV file: %v", err)
	}

	type callidFeedBack struct {
		AppID    string `json:"appID,omitempty"`
		Comment  string `json:"comment,omitempty"`
		Decision string `json:"decision,omitempty"`
		Time     string `json:"time,omitempty"`
		XcallID  string `json:"x_callID,omitempty"`
	}

	var feedBackData map[string]callidFeedBack
	var jio_transactions int = 0
	var jio_users_repeated []string

	ref2 := getgoxcallidClient().NewRef("/FeedBack-staging")
	error2 := ref2.Get(getContext(), &feedBackData)
	if error2 != nil {
		log.Fatal(error2)
		// fmt.Println(error2)
		// fmt.Println(feedBackData)

	}


	for i, _ := range callIDs {

		//fmt.Println(callIDs[i])


		fmt.Println(i,callIDs[i] )

		var feedback_json callidFeedBack
		feedback_json = feedBackData[callIDs[i]]

		fmt.Println("callid_feedback_json", feedback_json.Decision)

		var callid = callIDs[i]
		// fmt.Println(callid)
		// _ = os.MkdirAll("transactions/"+date+callid, 0777)
		type RetrieveDashboardOut struct {
			ID         string `json:"id,omitempty"`
			UserID     string `json:"userID,omitempty"`
			AppID      string `json:"appID,omitempty"`
			Path       string `json:"path,omitempty"`
			Env        string `json:"env,omitempty"`
			Properties struct {
				Path       string `json:"path,omitempty"`
				NewUser    int    `json:"newuser,omitempty"`
				StatusCode int    `json:"statusCode,omitempty"`
				// StatusCodeDB     int           `json:"statusCodeDB,omitempty"`
				AutoResp        int       `json:"autoResp,omitempty"`
				Error           string    `json:"error,omitempty"`
				FaceRatio       string    `json:"faceratio,omitempty"`
				FaceSpan        string    `json:"facespan,omitempty"`
				ObjDetSpan      string    `json:"objDetSpan,omitempty"`
				SpoofspanPpr    string    `json:"spoofspanPpr,omitempty"`
				SpoofSpanHr     string    `json:"spoofspanHr,omitempty"`
				SpoofSpanMac    string    `json:"spoofspanMac,omitempty"`
				SpoofspanScr    string    `json:"spoofspanScr,omitempty"`
				SpoofSpan       string    `json:"spoofspan,omitempty"`
				SpoofSpan2      string    `json:"spoofspan2,omitempty"`
				SpoofSpan3      string    `json:"spoofspan3,omitempty"`
				SpoofSpan4      string    `json:"spoofspan4,omitempty"`
				SpoofSpan3_30E  string    `json:"spoofspan3_30E,omitempty"`
				SpoofFiopSpan   string    `jsno:"Spooffiopspan,omitempty"`
				ProbabilitySpan string    `json:"probabilityspan,omitempty"`
				PeripheralSpan  string    `json:"PeripheralSpan,omitempty"`
				Latency         int       `json:"latency,omitempty"`
				StartTime       time.Time `json:"starttime,omitempty"`
				EndTime         time.Time `json:"endtime,omitempty"`
				Version         int       `json:"version,omitempty"`
				Distance        float64   `json:"distance"`
				TypeFrom        string    `json:"typefrom"`
				Probabilities   []float64 `json:"probabilities"`
				ProbMean        float64   `json:"probMean,omitempty"`
				LiveMean        float64   `json:"liveMean,omitempty"`
				LiveMean2       float64   `json:"liveMean2,omitempty"`
				LiveMean3       float64   `json:"liveMean3,omitempty"`
				LiveMean4       float64   `json:"liveMean4,omitempty"`
				LiveMean3_30E   float64   `json:"liveMean3_30E,omitempty"`
				LiveMeanHr      float64   `json:"liveMeanHr,omitempty"`
				LiveMeanMac     float64   `json:"liveMeanMac,omitempty"`
				LiveMeanPpr     float64   `json:"liveMeanPpr,omitempty"`
				LiveMeanPpr2    float64   `json:"liveMeanPpr2,omitempty"`
				LiveMeanScr     float64   `json:"liveMeanScr,omitempty"`
				UniMean	        float64   `json:"uniMean,omitempty"`
				PersonProb      float64   `json:"personProb,omitempty"`
				ScreenProb      float64   `json:"screeenProb,omitempty"`
				PrintProb       float64   `json:"printProb,omitempty"`
				FiopMean        float64   `json:"fiopMean,omitempty"`
				LiveOBD         string   `json:"liveOBD,omitempty"`
				// LiveMeanMob      float64   `json:"liveMeanMob"`
				RefMean          float64   `json:"refMean"`
				Blur             float64   `json:"blur"`
				ProbabilitiesRef []float64 `json:"probabilitiesref"`
				Landmarks        string    `json:"landmarks"`
				LandmarksDb      [][]int   `json:"landmarksdb"`
			} `json:"properties"`
		}
		var eachCallidData RetrieveDashboardOut
		var refpath string
		var ref_crop string
		var new_ref string
		var image1 string
		var image2 string
		var probabilities string
		var probabilities_ref string
		var prob_mean string
		var live_mean string
		var live_mean2 string
		var live_mean3 string
		var live_mean4 string
		var ref_mean string
		var distance string
		var resp_time string
		var resolution_ratio string
		var sTime string
		var eTime string
		var authCropResolution string
		var autoresp string
		var blur string
		var landmarks string
		var livemeanPpr string
		var livemeanScr string
		var livemeanHr string
		var livemean3_30E string
		var livemeanMac string
		var livemeanPpr2 string
		var unimean string
		var personProb string
		var screenProb string
		var printProb string
		var fiopMean string
		var liveOBD string


		// var livemeanMob string
		var user_feedback string = feedback_json.Decision
		// make a http get request to each xcallid
		var url string = "https://staging.vishwamcorp.com/v2/dashboard/"

		resp, err := http.Get(url + callid)
		if err != nil {
			fmt.Println(err)
		} else {

			body_values, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatal(err)
			}
			err2 := json.Unmarshal([]byte(body_values), &eachCallidData)
			if err != nil {
				fmt.Println(err2)
			}
			if eachCallidData.AppID == "jiophone" {
				jio_transactions += 1
				jio_users_repeated = append(jio_users_repeated, eachCallidData.UserID)

			}

			properties := eachCallidData.Properties
			//fmt.Println(resp.Body, resp.StatusCode)

			responsecode := properties.StatusCode

			//userIDs = append(userIDs,eachCallidData.UserID)
			// autoresp = strconv.Itoa(properties.autoResp)
			fmt.Println(responsecode)
			fmt.Println(eachCallidData.UserID, eachCallidData.Path)
			if eachCallidData.Path == "/v2/me/reference_ios" || eachCallidData.Path == "/v1/me/reference" {
				refpath = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image.png"
				ref_crop = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/ref_crop.png"
				new_ref = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/ref:" + eachCallidData.UserID
				image1 = ""
				image2 = ""
				ps, pm := getprobString(properties.Probabilities)
				probabilities = ps
				probabilities_ref = ""
				prob_mean = strconv.FormatFloat(pm, 'g', 6, 64)
				live_mean = strconv.FormatFloat(properties.LiveMean, 'g', 6, 64)
				ref_mean = strconv.FormatFloat(properties.RefMean, 'g', 6, 64)
				distance = ""
				resp_time = strconv.Itoa(properties.Latency)
				resolution_ratio = ""
				loc, _ := time.LoadLocation("Asia/Kolkata")
				startTime := properties.StartTime
				fmt.Println("UTC time:", startTime)
				sTime = startTime.In(loc).String()
				fmt.Println("start time:", sTime)
				endTime := properties.EndTime
				eTime = endTime.In(loc).String()
				authCropResolution = ""
				autoresp = strconv.Itoa(properties.AutoResp)
				blur = strconv.FormatFloat(properties.Blur, 'g', 6, 64)
				landmarks = ""
				// livemeanMob = ""
				livemeanPpr = ""
				livemeanPpr2 = ""
				livemeanScr = ""
				live_mean2 = ""
				live_mean3 = ""
				live_mean4 = ""
				livemeanHr = ""
				livemeanMac = ""
				livemean3_30E = ""
				unimean = ""
				personProb = ""
				printProb = ""
				screenProb = ""
				fiopMean = ""
				liveOBD = ""

			} else if eachCallidData.Path == "/v2/single_gesture_ios" || eachCallidData.Path == "/v1/single_gesture" {
				refpath = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/reference_image.png"
				image1 = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image.png"
				image2 = "http://staging.vishwamcorp.com/v2/traces/" + callid + "/image2.png"
				// full_resolution = ""
				// cropped_resolution = ""

				// fmt.Println(authCropResolution)
				authCropResolution = ""

				// if cropped_resolution == 0 || full_resolution == 0 {
				// 	resolution_ratio = "No images found"
				// } else {
				// 	fmt.Println("resolution:", full_resolution, cropped_resolution)
				// 	resolution_ratio = strconv.FormatFloat(float64(full_resolution)/float64(cropped_resolution), 'g', 6, 64)
				// }
				resolution_ratio = properties.FaceRatio
				// if responsecode == 200 {
				// 	_ = os.MkdirAll("transactions/"+date+"/"+callid, 0777)
				// 	refFileName := "ref_full.png"
				// 	saveImage("transactions/"+date+"/"+callid+"/"+refFileName, refpath)
				// 	authFileName := "auth_full.png"
				// 	saveImage("transactions/"+date+"/"+callid+"/"+authFileName, image1)
				// }

				ref_crop = "https://staging.vishwamcorp.com/v2/traces/" + callid + "/ref_crop.png"
				new_ref = "https://staging.vishwamcorp.com/v2/traces/" + callid + "/ref:" + eachCallidData.UserID
				ps, pm := getprobString(properties.Probabilities)
				probabilities = ps
				psr, pmr := getprobString(properties.ProbabilitiesRef)
				probabilities_ref = psr
				prob_mean = strconv.FormatFloat(pm, 'g', 6, 64)
				live_mean = strconv.FormatFloat(properties.LiveMean, 'g', 6, 64)
				ref_mean = strconv.FormatFloat(pmr, 'g', 6, 64)
				distance = strconv.FormatFloat(properties.Distance, 'g', 6, 64)
				resp_time = strconv.Itoa(properties.Latency)
				loc, _ := time.LoadLocation("Asia/Kolkata")
				startTime := properties.StartTime
				fmt.Println("UTC time:", startTime)
				sTime = startTime.In(loc).String()
				fmt.Println("start time:", sTime)
				endTime := properties.EndTime
				eTime = endTime.In(loc).String()

				autoresp = strconv.Itoa(properties.AutoResp)
				blur = strconv.FormatFloat(properties.Blur, 'g', 6, 64)
				landmarks = properties.Landmarks
				// livemeanMob = strconv.FormatFloat(properties., 'g', 6, 64)
				// fmt.Println(properties)
				livemeanPpr = strconv.FormatFloat(properties.LiveMeanPpr, 'g', 6, 64)
				livemeanPpr2 = strconv.FormatFloat(properties.LiveMeanPpr2, 'g', 6, 64)
				livemeanScr = strconv.FormatFloat(properties.LiveMeanScr, 'g', 6, 64)
				live_mean2 = strconv.FormatFloat(properties.LiveMean2, 'g', 6, 64)
				live_mean3 = strconv.FormatFloat(properties.LiveMean3, 'g', 6, 64)
				live_mean4 = strconv.FormatFloat(properties.LiveMean4, 'g', 6, 64)
				livemeanHr = strconv.FormatFloat(properties.LiveMeanHr, 'g', 6, 64)
				livemeanMac = strconv.FormatFloat(properties.LiveMeanMac, 'g', 6, 64)
				livemean3_30E = strconv.FormatFloat(properties.LiveMean3_30E, 'g', 6, 64)
				unimean =  strconv.FormatFloat(properties.UniMean, 'g', 6, 64)
				personProb =  strconv.FormatFloat(properties.PersonProb, 'g', 6, 64)
				screenProb =  strconv.FormatFloat(properties.ScreenProb, 'g', 6, 64)
				printProb =  strconv.FormatFloat(properties.PrintProb, 'g', 6, 64)
				fiopMean = strconv.FormatFloat(properties.FiopMean, 'g', 6, 64)
				liveOBD = properties.LiveOBD
			}

			eachCID := []string{strconv.Itoa(i + 1), sTime, eTime, callid, eachCallidData.UserID, eachCallidData.AppID, eachCallidData.Path, refpath, ref_crop, new_ref, image1, image2, authCropResolution, resolution_ratio, strconv.Itoa(responsecode), autoresp, resp_time, probabilities, probabilities_ref, prob_mean, live_mean, live_mean2, live_mean3, live_mean4, livemean3_30E, livemeanHr, livemeanMac, livemeanPpr, livemeanPpr2, livemeanScr, ref_mean,unimean,personProb,screenProb,printProb,fiopMean,liveOBD, distance, blur, landmarks, properties.FaceSpan, properties.SpoofSpan, properties.SpoofSpan2, properties.SpoofSpan3, properties.SpoofSpan4, properties.SpoofSpan3_30E, properties.SpoofSpanHr, properties.SpoofSpanMac, properties.SpoofspanScr, properties.SpoofspanPpr, properties.ProbabilitySpan, properties.SpoofFiopSpan, user_feedback, properties.PeripheralSpan, properties.TypeFrom}

			//fmt.Println(eachCID)
			//resp := hitApi(ref_crop,image2,eachCallidData.UserID)
			//fmt.Println(resp)
			tracesData = append(tracesData, eachCID)

		}



	}

	fmt.Println(len(callIDs))


	reportname := "17janModel.csv"
	file, err := os.Create( "public/csv_reports/" +reportname)
	checkError("Cannot create file", err)
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for index, _ := range tracesData {
		err := writer.Write(tracesData[index])
		checkError("Cannot write to file", err)
	}

	fmt.Println("Report Generated")

}

func replaceUserIds() {

	fmt.Println("started reading csv file")

	var userIDs []string
	r := easycsv.NewReaderFile("public/csv_reports/new.csv")
	var change struct {
		OldUser string `index:"0"`
		//NewUser string `index:"1"`
	}

	for r.Read(&change) {


		userIDs = append(userIDs, change.OldUser)

	}

	if err := r.Done(); err != nil {
		log.Fatalf("Failed to read a CSV file: %v", err)
	}

	//for i, _ := range userIDs {
	//
	//	fmt.Println(userIDs[i][0])
	//	fmt.Println(makeKeyReferenceImage(userIDs[i][0]))
	//
	//	downloadImage("public/users/"+userIDs[i][0], makeKeyReferenceImage(userIDs[i][0]))
	//	uploadImage("public/users/"+userIDs[i][0], makeKeyReferenceImage(userIDs[i][1]))
	//
	//}

	fmt.Println(len(userIDs))
}

func makeKeyReferenceImage(userID string) string {
	group := userID
	if len(userID) >= 2 {
		group = userID[0:2]
	}

	return filepath.Join(group, userID)
}

func main() {

	date := os.Args[1]
	fmt.Println(date)
	// getTransactionsReport(date)
	// downloadImages(date)
	getAllImagesList(date)
	//var name string = "nikhil"
	//fmt.Println(name[:3])
	//getTotalTransactions(date)
	//generateModelsReport()
	// getAll(date)
	//replaceUserIds()
	// readcsvFile()
}

